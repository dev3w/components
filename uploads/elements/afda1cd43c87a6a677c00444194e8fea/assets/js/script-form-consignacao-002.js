jQuery(document).ready(function() {

    var navListItems = jQuery('ul.setup-panel li a'),
        allWells = jQuery('.consignacaoform');

    allWells.hide();

    navListItems.click(function(e) {
        e.preventDefault();
        var jQuerytarget = jQuery(jQuery(this).attr('href')),
            jQueryitem = jQuery(this).closest('li');

        if (!jQueryitem.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active background-theme');
            jQueryitem.addClass('active background-theme');
            allWells.hide();
            jQuerytarget.show();
        }
    });

    jQuery('ul.setup-panel li.active a').trigger('click');

    jQuery('#activate-step-2').on('click', function(e) {
        jQuery('ul.setup-panel li:eq(1)').removeClass('disabled');
        jQuery('ul.setup-panel li a[href="#step-2"]').trigger('click');
        //jQuery(this).remove();
    })
    jQuery('#activate-step-1').on('click', function(e) {
        jQuery('ul.setup-panel li:eq(1)').removeClass('disabled');
        jQuery('ul.setup-panel li a[href="#step-1"]').trigger('click');
        //jQuery(this).remove();
    })
});