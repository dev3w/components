
(function ($) {

    var itemGlobal =   $('.slider_s0080 #myCarousel_s0080');
    itemGlobal.carousel({
        interval: false
    });

    var callback = function () {

        itemGlobal.carousel('next');

    }

    initSlideBootstrap(itemGlobal,window.guid(), callback);

}(jQuery))
