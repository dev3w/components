
(function ($) {

    var itemGlobal =   $('.slider-s002 #bootstrap-touch-slider');
    itemGlobal.carousel({
        interval: false
    });

    var callback = function () {

        itemGlobal.carousel('next');

    }

      initSlideBootstrap(itemGlobal, window.guid(),callback);

}(jQuery))
