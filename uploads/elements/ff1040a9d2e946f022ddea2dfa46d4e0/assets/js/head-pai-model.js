(function($) {

    var logo = $('.head-pai-model .logo');
    var menu = $('.head-pai-model .nav.navbar-nav');
    var windowSize = $(window).width();
    var navBar = $('.head-pai-model .navbar-collapse');
    var imgLogo = $('.head-pai-model .img-logo');


    $('.head-pai-model .navbar-toggle').on('click', function(e) {
        navBar.toggleClass('active');
    });
    $(document).ready(function() {
        localStorage.setItem('tamanhoLogo', $('.head-pai-model .logo').width());
        localStorage.setItem('paddingMenu', menu.css('padding-top'));
        var logo = $('.head-pai-model .logo').clone();
        imgLogo.append(logo);

    });

    var changeMenu = function(elemUm, elemDois) {
        var logoTamanho = elemDois.width();
        elemUm.width('30%');
        elemDois.css('padding-top', '20px');

    }
    var restoreMenu = function() {
        logo.width(localStorage.getItem('tamanhoLogo'));
        menu.css('padding-top', localStorage.getItem('paddingMenu'));
    }

    $(window).resize(function() {
        windowSize = $(window).width();
    });

    $(window).on('scroll', function() {
        var scrollY = navBar.offset();
        if (scrollY.top > 10 && windowSize > 1199) {
            changeMenu(logo, menu);

        } else {
            restoreMenu();
        }
    });

}(jQuery))