$('.ofertasdestaque-o004 .center').slick({
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 4,
    variableImg: true,
    responsive: [{
        breakpoint: 991,
        settings: {
            slidesToShow: 1,
            arrows: false

        }
    }]

});