jQuery(document).ready(function() {
    jQuery('.destaque_old_mod_003 .slider-destaque').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        arrows: false,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    dots: false,

                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    dots: false,

                }
            }
        ]
    });

})