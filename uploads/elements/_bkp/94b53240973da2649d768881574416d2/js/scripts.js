jQuery(document).ready(function() {

    jQuery('.aba-a003 .tabbed-content').each(function() {
        jQuery(this).append('<ul class="content"></ul>');
    });

    jQuery('.aba-a003 .tabs li').each(function() {
        var originalTab = jQuery(this),
            activeClass = "";
        if (originalTab.is('.tabs>li:first-child')) {
            activeClass = ' class="active"';
        }
        var tabContent = originalTab.find('.tab-content').detach().wrap('<li' + activeClass + '></li>').parent();
        originalTab.closest(' .tabbed-content').find('.content').append(tabContent);
    });

    jQuery('.aba-a003 .tabs li').click(function() {
        jQuery(this).closest('.tabs').find('li').removeClass('active');
        jQuery(this).addClass('active');
        var liIndex = jQuery(this).index() + 1;
        jQuery(this).closest('.tabbed-content').find('.content>li').removeClass('active');
        jQuery(this).closest('.tabbed-content').find('.content>li:nth-of-type(' + liIndex + ')').addClass('active');
    });
});