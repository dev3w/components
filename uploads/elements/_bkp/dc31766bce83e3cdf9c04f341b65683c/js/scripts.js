
function changeWebhubElementUI() {
    jQuery(".form-teste-drive-001 .selector select").each(function() {
        var obj = jQuery(this);
        if (obj.parent().children(".custom-select").length < 1) {
            obj.after("<span class='custom-select'>" + obj.children("option:selected").html() + "</span>");

            if (obj.hasClass("white-bg")) {
                obj.next("span.custom-select").addClass("white-bg");
            }
            if (obj.hasClass("full-width")) {
                obj.next("span.custom-select").addClass("full-width");
            }
        }
    });
    jQuery("body").on("change", ".form-teste-drive-001 .selector select", function() {
        if (jQuery(this).next("span.custom-select").length > 0) {
            jQuery(this).next("span.custom-select").text(jQuery(this).find("option:selected").text());
        }
    });

    jQuery("body").on("keydown", ".form-teste-drive-001 .selector select", function() {
        if (jQuery(this).next("span.custom-select").length > 0) {
            jQuery(this).next("span.custom-select").text(jQuery(this).find("option:selected").text());
        }
    });

    try {
        jQuery('.form-teste-drive-001 input, textarea').placeholder();
    } catch (e) {}
}

jQuery(document).ready(function() {
    changeWebhubElementUI();

});