jQuery(document).ready(function() {

    // ..site header drop down menu...
    jQuery(window).click(function() {
        jQuery(".x_component_header_cod_200 .click-toggle-menu").removeClass('open');
    });

    jQuery(".x_component_header_cod_200 .click-toggle-menu > a").click(function(event) {
        event.stopPropagation();
        jQuery(this).parent().siblings('.click-toggle-menu').removeClass('open');
        jQuery(this).parent().toggleClass('open');
    });

    //mobile menu
    jQuery('.x_component_header_cod_200 .mobile-button').click(function() {
        jQuery(this).toggleClass('active');
        jQuery(this).parent().siblings('#navbar').toggleClass('menu-toggle');
    });



    /*...site navigation menu...*/
    jQuery("#navbar > ul").each(function() {
        jQuery(this).addClass('nav navbar-nav navbar-right');
        jQuery(this).find('ul > li').each(function() {
            jQuery(this).parent().parent().addClass("dropdown");
            jQuery(this).parent().parent().children('a').addClass("dropdown-toggle");
            jQuery(this).parent().parent().children('a').attr('data-toggle', 'dropdown');
            jQuery(this).parent().parent().children('a').attr('role', 'button');
            jQuery(this).parent().parent().children('a').attr('aria-haspopup', 'true');
            jQuery(this).parent().parent().children('a').attr('aria-expanded', 'false');
            jQuery(this).parent().addClass('dropdown-menu');

        });
    });

});