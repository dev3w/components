jQuery(document).ready(function() {

    jQuery('.highlights-h016 .center').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        responsive: [{
                breakpoint: 1090,
                settings: {
                    arrows: false,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    slidesToShow: 1
                }
            }
        ]
    });

});