( function( $ ){
    $(document).ready(function() {
        
        $('.x_component_slider_cod_306 #slide-nav').slick();

        $('.x_component_slider_cod_306 #slides li img').load(function() {

            $('.x_component_slider_cod_306 #slide-nav').slick('unslick').html('');

            var slideContent = $('.x_component_slider_cod_306 #slides li:not(".slick-cloned")').clone();

            $('.x_component_slider_cod_306 #slide-nav').append(slideContent);

            $('.x_component_slider_cod_306 #slide-nav').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '#slides',
                arrows: false,
                centerMode: true,
                focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 460,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ]
            });

            $('.x_component_slider_cod_306 #slide-nav li').prepend('<span class="background-theme"></span>');

        });

        $('.x_component_slider_cod_306 #slides').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: '#slide-nav'
        });
    });
})( jQuery );