(function($) {
    function removeClassElement() {
        $('.x_component_work_code_146  .mb-0').children('.collapsed').removeClass('active');
        $('.x_component_work_code_146  .mb-0').parent().siblings().removeClass('show');
    }
    $('.x_component_work_code_146  .mb-0').on('click', function() {
        if ($(this).children('.collapsed').hasClass('active')) {
            removeClassElement();
        } else {
            removeClassElement();
            $(this).children('.collapsed').addClass('active');
            $(this).parent().siblings().addClass('show');
        }
    })
})(jQuery)