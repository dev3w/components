jQuery(document).ready(function() {
    jQuery(".menu-m002 .cont-menu > ul").addClass("menu");
    jQuery(".menu-m002 .cont-menu > ul > li > ul").addClass("mega-menu");
    jQuery(".menu-m002 .cont-menu > ul > li").each(function() {
        jQuery(this).find('ul').each(function() {
            if (jQuery(this).parent('li').length > 0) {
                jQuery(this).parent('li').addClass("has-dropdown");
            }
        });
    });

    jQuery('.menu-m002 .mobile-toggle').click(function() {
        jQuery('.nav-bar').toggleClass('nav-open');
        jQuery(this).toggleClass('active');
    });

    jQuery('.menu-m002 .menu > li.has-dropdown > a').click(function(e) {
        if (!e) e = window.event;
        e.preventDefault();
    });

    jQuery('.menu-m002 .menu > li.has-dropdown').mouseover(function() {
        jQuery(this).addClass('open');
        jQuery(this).find('a').attr('aria-expanded', true);
    })
    .mouseout(function() {
        jQuery(this).removeClass('open');
        jQuery(this).find('a').attr('aria-expanded', false);
    });

    jQuery('.menu-m002 .menu li > a').click(function() {
        if (jQuery(this).hasClass('inner-link')) {
            jQuery(this).closest('.nav-bar').removeClass('nav-open');
        }
    });

    jQuery('.menu-m002 .module.widget-handle').click(function() {
        jQuery(this).toggleClass('toggle-widget-handle');
    });

    jQuery('.menu-m002 .search-widget-handle .search-form input').click(function(e) {
        if (!e) e = window.event;
        e.stopPropagation();
    });
});
