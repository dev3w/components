jQuery(document).ready(function() {
    jQuery(".menu-m006 .cont-menu > ul").addClass("menu");
    jQuery(".menu-m006 .cont-menu > ul > li > ul").addClass("mega-menu");
    jQuery(".menu-m006 .cont-menu > ul > li").each(function() {
        jQuery(this).find('ul').each(function() {
            if (jQuery(this).parent('li').length > 0) {
                jQuery(this).parent('li').addClass("has-dropdown");
            }
        });
    });

    jQuery('.menu-m006 .mobile-toggle').click(function() {
        jQuery('.nav-bar').toggleClass('nav-open');
        jQuery(this).toggleClass('active');
    });

    jQuery('.menu-m006 .menu li').click(function(e) {
        if (!e) e = window.event;
        e.stopPropagation();
        if (jQuery(this).find('ul').length) {
            jQuery(this).toggleClass('toggle-sub');
        } else {
            jQuery(this).parents('.toggle-sub').removeClass('toggle-sub');
        }
    });

    jQuery('.menu-m006 .menu li > a').click(function() {
        if (jQuery(this).hasClass('inner-link')) {
            jQuery(this).closest('.nav-bar').removeClass('nav-open');
        }
    });

    jQuery('.menu-m006 .module.widget-handle').click(function() {
        jQuery(this).toggleClass('toggle-widget-handle');
    });

    jQuery('.menu-m006 .search-widget-handle .search-form input').click(function(e) {
        if (!e) e = window.event;
        e.stopPropagation();
    });

});