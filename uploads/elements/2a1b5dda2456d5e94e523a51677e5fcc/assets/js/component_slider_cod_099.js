

(function ($) {

    var itemGlobal =   $('.x_component_slider_cod_099 #carousel-generic');
    itemGlobal.carousel({
        interval: false
    });

    var callback = function () {

        itemGlobal.carousel('next');

    }

    initSlideVideoBootstrap(itemGlobal, callback);

}(jQuery))