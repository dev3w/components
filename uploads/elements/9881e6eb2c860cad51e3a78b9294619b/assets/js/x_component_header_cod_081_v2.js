jQuery(document).ready(function() {

    $(window).resize(function() {
        loadDropDownMenu();
    });
    if (jQuery(window).width() > 768) {
        loadDropDownMenu();
    }

    if (jQuery(window).width() < 768) {
        jQuery('.x_component_header_cod_081_v2 .navbar').addClass('navbar-xs');
    }
    jQuery('.x_component_header_cod_081_v2 .navbar-ex1-collapse').addClass('navbar-collapsed-from');
    jQuery('.x_component_header_cod_081_v2 .navbar-toggle').on('click', function() {
        var data = jQuery('.x_component_header_cod_081_v2 .navbar-ex1-collapse').attr('aria-expanded');
        if (data === 'true') {
            jQuery('.x_component_header_cod_081_v2 .navbar-toggle').removeClass('active');
            jQuery('.x_component_header_cod_081_v2 .navbar-ex1-collapse').addClass('collapse-drawer');
            jQuery('.x_component_header_cod_081_v2 .navbar-ex1-collapse').removeClass('navbar-collapsed-from');
            jQuery('.x_component_header_cod_081_v2 .navbar-ex1-collapse').addClass('navbar-collapsed-to');
        } else {
            jQuery('.x_component_header_cod_081_v2 .navbar-ex1-collapse').removeClass('collapse-drawer');
            jQuery('.x_component_header_cod_081_v2 .navbar-ex1-collapse').removeClass('navbar-collapsed-to');
            jQuery('.x_component_header_cod_081_v2 .navbar-ex1-collapse').addClass('navbar-collapsed-from');
            jQuery('.x_component_header_cod_081_v2 .navbar-toggle').addClass('active');
        }
    });


    function loadDropDownMenu() {
        if ($(window).width() > 767) {
            $(document).on({
                mouseenter: function() {
                    $(this).find('>.dropdown-menu').stop(true, true).delay(200).slideDown(500).fadeIn(500);
                    $(this).children('a').addClass('hovered')
                },
                mouseleave: function() {
                    $(this).children('a').removeClass('hovered');
                    $(this).find('>.dropdown-menu').stop(true, true).delay(200).slideUp(500).fadeOut(500);
                }
            }, ".x_component_header_cod_081_v2 ul.nav li.dropdown");

            $(document).on({
                mouseenter: function() {
                    $(this).find('.dropdown-menu').slideDown(500).fadeIn(500);
                    $(this).children('a').addClass('hovered')
                },
                mouseleave: function() {
                    $(this).find('.dropdown-menu').slideUp(500).fadeOut(500);
                    $(this).children('a').removeClass('hovered');

                }
            }, ".x_component_header_cod_081_v2 ul.nav li.dropdown .dropdown-submenu");
        }
    }

    $(".x_component_header_cod_081_v2 .navbar-ex1-collapse > ul").each(function() {
        $(this).addClass('nav navbar-nav navbar-right');
        $(this).find('ul > li').each(function() {
            if (!$(this).parent().parent().hasClass('dropdown-submenu')) {
                $(this).parent().parent().addClass("dropdown");
            }
            $(this).parent().parent().children('a').addClass("dropdown-toggle");
            $(this).parent().parent().children('a').attr('data-toggle', 'dropdown');
            $(this).parent().parent().children('a').attr('role', 'button');
            $(this).parent().parent().children('a').attr('aria-haspopup', 'true');
            $(this).parent().parent().children('a').attr('aria-expanded', 'false');
            $(this).parent().addClass('dropdown-menu');
            $(this).find('ul > li').each(function() {
                $(this).parent().parent().removeClass('dropdown-menu').addClass('dropdown-submenu');
            });
        });
        $(this).find('ol').each(function() {
            $(this).parent().parent().parent().addClass('no-reference');
            $(this).parent().parent().addClass("dropdown-menu mega-menu");
            $(this).parent().parent().children('a').addClass("dropdown-toggle");
            $(this).parent().parent().children('a').attr('data-toggle', 'dropdown');
            $(this).parent().parent().children('a').attr('role', 'button');
            $(this).parent().parent().children('a').attr('aria-haspopup', 'true');
            $(this).parent().parent().children('a').attr('aria-expanded', 'false');
            $(this).parent().addClass('mega-menu-column');
        });
    });
});