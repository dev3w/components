(function($) {

    var buildMenuMobile = function() {
        var menu = $('.componente_header_menu-16_v2 .menu');
        var menuSize = menu.children('li');
        var mobileMenu = $('.componente_header_menu-16_v2 .mobile-menu select');

        menuSize.each(function(i) {
            mobileMenu.append('<option value="' + $('>a', this).attr('href') + '">' + $('>a', this).text() + '</option>');
            var subMenuSize = $(this).children('ul').children('li').size();
            var subMenu = $(this).children('ul').children('li');
            subMenu.each(function() {
                if (subMenuSize > 1) {
                    mobileMenu.append('<option value="' + $('a', this).attr('href') + '">-' + $('a', this).text() + '</option>');
                }
            })
        })
    }
    buildMenuMobile();
    var animaSubmenu = function() {

        var subMenu = $('.componente_header_menu-16_v2 .menu li.nav-menu-item');

        console.log(subMenu);
        subMenu.mouseenter(function() {
            console.log('passou');
            tamanhoMenu($(this))
        })
        subMenu.mouseleave(function() {
            $(this).children('ul').height(0)
        })
    }
    animaSubmenu();
    var redirect = function() {
        $('.componente_header_menu-16_v2 .mobile-menu select').on('change', function() {
            window.location.href = $(this).val();
        })
    }
    redirect();
    var tamanhoMenu = function(element) {
        console.log('tamanho menu')
        var subMenu = $('ul', $(element));
        subMenu.each(function() {
            var childSize = $(this).children().size();
            var childHeight = $(this).children().height();
            $(this).height(childSize * childHeight);
        })
    }
}(jQuery))