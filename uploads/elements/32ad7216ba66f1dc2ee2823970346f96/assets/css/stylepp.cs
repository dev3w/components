.componente_header_menu-16_v2-padding-default,
.componente_header_menu-16_v2-pull-left,
.componente_header_menu-16_v2-pull-right {
  padding-left: 15px;
  padding-right: 15px;
}
@media (max-width: 768px) {
  .componente_header_menu-16_v2-padding-default,
  .componente_header_menu-16_v2-pull-left,
  .componente_header_menu-16_v2-pull-right {
    padding: 0px;
  }
}
.componente_header_menu-16_v2-pull-left {
  float: left;
}
.componente_header_menu-16_v2-pull-right {
  float: right;
}
.componente_header_menu-16_v2 .reset-border {
  border: 0px;
  border-radius: 0px;
  border: none;
}
.componente_header_menu-16_v2 .mobile-menu {
  display: none;
}
@media (max-width: 768px) {
  .componente_header_menu-16_v2 .mobile-menu {
    display: block;
  }
}
.componente_header_menu-16_v2 .navbar-default-custom {
  padding: 10px 0px 0px 0px;
  background: red;
}
@media (max-width: 768px) {
  .componente_header_menu-16_v2 .navbar-default-custom .container-custom {
    padding: 0px;
  }
}
.componente_header_menu-16_v2 .navbar-default-custom .container-custom > .navbar-header-custom {
  width: 100%;
  margin-bottom: 0px;
}
@media (max-width: 768px) {
  .componente_header_menu-16_v2 .navbar-default-custom .container-custom > .navbar-header-custom {
    margin: 0px;
  }
}
.componente_header_menu-16_v2 .navbar-default-custom .info-group {
  padding-top: 15px;
}
.componente_header_menu-16_v2 .navbar-default-custom .info-group ul {
  list-style: none;
  padding: 0px;
}
.componente_header_menu-16_v2 .navbar-default-custom .info-group ul li {
  padding-right: 10px;
  display: inline-block;
}
.componente_header_menu-16_v2 .navbar-default-custom .info-group ul li div {
  display: inline;
}
.componente_header_menu-16_v2 .navbar-default-custom .menu {
  background: #ccc;
  float: left;
  width: 100%;
  padding: 10px 0px;
  margin-bottom: 0px;
}
.componente_header_menu-16_v2 .navbar-default-custom .menu li {
  display: inline-block;
}
.componente_header_menu-16_v2 .navbar-default-custom .menu li a {
  display: block;
  padding: 5px 15px;
}
.componente_header_menu-16_v2 .navbar-default-custom .menu li a:hover {
  background: blue;
}
@media (max-width: 768px) {
  .componente_header_menu-16_v2 .navbar-default-custom .menu li {
    text-align: center;
    width: 100%;
  }
}
@media (max-width: 768px) {
  .componente_header_menu-16_v2 .navbar-default-custom .menu {
    display: none;
  }
}
.componente_header_menu-16_v2 .navbar-custom .logo img {
  width: 100%;
}
@media (max-width: 768px) {
  .componente_header_menu-16_v2 .navbar-custom .logo {
    width: 100%;
    text-align: center;
  }
  .componente_header_menu-16_v2 .navbar-custom .logo img {
    float: none;
    width: auto;
  }
}
