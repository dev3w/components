 var key = "AIzaSyAzR6gkmpa-7k9p-fO0-YfZBkKfNX9Na_I";
    var maps = $('script[src*="maps.googleapis.com/maps/api/js?libraries=places&key='+key+'&sensor=false"]').length;
    if(maps == 0) {
        var script = document.createElement('script');
        script.src = "//maps.googleapis.com/maps/api/js?libraries=places&key="+key+"&sensor=false";
        document.body.appendChild(script);

        var script = document.createElement('script');
        script.src = "/assets/themes/default/js/map.js";
        document.body.appendChild(script);
    }

jQuery(document).ready(function() {
    jQuery('.aba-a006 .tabbed-content').each(function() {
        jQuery(this).append('<ul class="content"></ul>');
    });

    jQuery('.aba-a006 .tabs li').each(function() {
        var originalTab = jQuery(this),
            activeClass = "";
        if (originalTab.is('.tabs>li:first-child')) {
            activeClass = ' class="active"';
        }
        var tabContent = originalTab.find('.tab-content').detach().wrap('<li' + activeClass + '></li>').parent();
        originalTab.closest(' .tabbed-content').find('.content').append(tabContent);
    });

    jQuery('.aba-a006 .tabs li').click(function() {
        jQuery(this).closest('.tabs').find('li').removeClass('active');
        jQuery(this).addClass('active');
        var liIndex = jQuery(this).index() + 1;
        jQuery(this).closest('.tabbed-content').find('.content>li').removeClass('active');
        jQuery(this).closest('.tabbed-content').find('.content>li:nth-of-type(' + liIndex + ')').addClass('active');
    });
  // gera as tabs
  	function geraTabs(){
      var abas = $('.aba-a006');
      for(var i = 0; i< abas.length;i++){
          var aba = abas[i];
     	  var map = $(aba).find('[data-type="map"]');
          var enderecos = map.attr("data-mapa").split("_");
          var link = $(aba).find('.tab-title')[0];
          var container = $(link).parent();
          $(container).empty();

          for(var j = 0; j < enderecos.length; j++){
              var endereco = enderecos[j];
              var linkClone = $(link).clone(true);
              $(linkClone.find("div")[0]).text(endereco);
              container.append(linkClone);
          }
      }
    }

	geraTabs();

	$('#saveByAddress').on('click', function(){
      geraTabs();
  	});
  
  $(document).on('click','.aba-a006 .tab-title',function(){
  	var mapOptions = {
        mapTypeId: 'roadmap'
    };
    var map = $(this).parent().parent().parent().find('[data-type="map"]')[0];
	var enderecos = $(map).attr("data-mapa").split("_");
    initGoogleMap(enderecos,map,$(this).find("div").text());
  });
});