jQuery(document).ready(function() {
    jQuery(window).click(function() {
        jQuery(".x_component_header_cod_301 .click-toggle-menu").removeClass('open');
    });

    jQuery(".x_component_header_cod_301 .click-toggle-menu > a").click(function(event){
        event.stopPropagation();
        jQuery(this).parent().siblings('.click-toggle-menu').removeClass('open');
        jQuery(this).parent().toggleClass('open');
    });

    jQuery(".x_component_header_cod_301 .top-search").click(function(event){
        event.stopPropagation();
        jQuery(this).parent().siblings('.box-cart').find('#cart').removeClass('open');
        jQuery(this).parent().toggleClass('open');
    });
    jQuery('.x_component_header_cod_301 #search').click(function(event){
        event.stopPropagation();
    });


    jQuery('.x_component_header_cod_301 #cart > button').addClass('dropdown');
    jQuery('.x_component_header_cod_301 #cart > ul').addClass('dropdown-menu');
    jQuery('.x_component_header_cod_301 .box-cart').click(function(){
        jQuery(this).siblings('.search_box').removeClass('open');
    });   



    jQuery(".x_component_header_cod_301 .top-navigation .navbar-collapse > ul").each(function() {
        jQuery(this).addClass('x_component_header_cod_301 nav navbar-nav hidden-xs');
        jQuery(this).siblings('div').find('ul').addClass('top-navigation-mobile-nav');
        jQuery(this).find('ul > li').each(function() {
            jQuery(this).parent().parent().addClass("dropdown");
            jQuery(this).parent().parent().children('a').addClass("dropdown-toggle");
            jQuery(this).parent().parent().children('a').attr('data-toggle','dropdown');
            jQuery(this).parent().parent().children('a').attr('role','button');
            jQuery(this).parent().parent().children('a').attr('aria-haspopup','true');
            jQuery(this).parent().parent().children('a').attr('aria-expanded','false');
            jQuery(this).parent().addClass('dropdown-menu');

        });
    });


    jQuery(".x_component_header_cod_301 #navbar > ul").each(function() {
        jQuery(this).addClass('x_component_header_cod_301 nav navbar-nav');
        jQuery(this).find('>li div div').each(function() {
            jQuery(this).addClass("dropdown-menu-inner");
            jQuery(this).parent().parent().addClass("dropdown");
            jQuery(this).parent().parent().children('a').addClass("dropdown-toggle");
            jQuery(this).parent().parent().children('a').attr('data-toggle','dropdown');
            jQuery(this).parent().parent().children('a').attr('role','button');
            jQuery(this).parent().parent().children('a').attr('aria-haspopup','true');
            jQuery(this).parent().parent().children('a').attr('aria-expanded','false');
            jQuery(this).parent().addClass('dropdown-menu');

        });
    });


    jQuery('.x_component_header_cod_301 .top-nav .navbar-header button').click(function(){
        jQuery(this).toggleClass('active');
        jQuery(this).parent().siblings('.collapse').toggleClass('open');
    });


    jQuery('.x_component_header_cod_301 .mobile-button').click(function(){
        jQuery(this).toggleClass('active');
        jQuery(this).parent().siblings('#navbar').slideToggle();
    });

});