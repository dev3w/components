jQuery(document).ready(function() {

    jQuery(".menu-m015 .cont-menu > ul").addClass("nav navbar-nav ");
    jQuery(".menu-m015 .cont-menu > ul > li").each(function() {
        jQuery(this).find('ul').each(function() {
            if (jQuery(this).parent('li').length > 0) {
                jQuery(this).addClass('dropdown-menu');
                jQuery(this).parent('li').addClass("dropdown");
                jQuery(this).parent('li').children('a').addClass('dropdown-toggle ').attr('data-toggle', 'dropdown');
            }
        });
    });
})