

(function ($) {

    var itemGlobal =   $('.x_component_slider_cod_065 #carousel-generic');
    itemGlobal.carousel({
        interval: false
    });

    var callback = function () {

        itemGlobal.carousel('next');

    }

    initSlideBootstrap(itemGlobal, window.guid(), callback);

}(jQuery))