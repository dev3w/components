jQuery(document).ready(function() {
    jQuery(".menu-m00g40 .cont-menu > ul").addClass("menu");
    jQuery(".menu-m00g40 .cont-menu > ul > li > ul").addClass("mega-menu");
    jQuery(".menu-m00g40 .cont-menu > ul > li").each(function() {
        jQuery(this).find('ul').each(function() {
            if (jQuery(this).parent('li').length > 0) {
                jQuery(this).parent('li').addClass("has-dropdown");
            }
        });
    });

    jQuery('.menu-m00g40 .mobile-toggle').click(function() {
      console.log(" click");
        jQuery('.nav-bar').toggleClass('nav-open');
        jQuery(this).toggleClass('active');
    });

    
});