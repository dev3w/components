jQuery(document).ready(function() {

    $('.x_component_header_cod_064 #bs-example-navbar-collapse-1 li').on('click', function(e){

        e.stopPropagation();

    });


    if (jQuery(window).width() < 768) {
        jQuery('.x_component_header_cod_064 .navbar').addClass('navbar-xs');
    }

    jQuery('.x_component_header_cod_064 #bs-example-navbar-collapse-1').addClass('navbar-collapsed-from');
    jQuery('.x_component_header_cod_064 .navbar-toggle').on('click', function() {

        var data = jQuery('.x_component_header_cod_064 #bs-example-navbar-collapse-1').attr('aria-expanded');
        if (data === 'true') {
            jQuery('.x_component_header_cod_064 .navbar-toggle').removeClass('active');
            jQuery('.x_component_header_cod_064 #bs-example-navbar-collapse-1').addClass('collapse-drawer');
            jQuery('.x_component_header_cod_064 #bs-example-navbar-collapse-1').removeClass('navbar-collapsed-from');
            jQuery('.x_component_header_cod_064 #bs-example-navbar-collapse-1').addClass('navbar-collapsed-to');
        } else {
            jQuery('.x_component_header_cod_064 #bs-example-navbar-collapse-1').removeClass('collapse-drawer');
            jQuery('.x_component_header_cod_064 #bs-example-navbar-collapse-1').removeClass('navbar-collapsed-to');
            jQuery('.x_component_header_cod_064 #bs-example-navbar-collapse-1').addClass('navbar-collapsed-from');
            jQuery('.x_component_header_cod_064 .navbar-toggle').addClass('active');
        }
    });


    jQuery(".x_component_header_cod_064 #bs-example-navbar-collapse-1 > ul").each(function() {
        jQuery(this).addClass('nav navbar-nav navbar-right menu');
        jQuery(this).find('ul > li').each(function() {
            jQuery(this).parent().parent().addClass("dropdown");
            jQuery(this).parent().parent().append('<i class="fa fa-chevron-down visible-xs mbl-drop-icon" aria-hidden="true"></i>');
            jQuery(this).parent().parent().children('a').addClass("dropdown-toggle");
            jQuery(this).parent().parent().children('a').attr('data-toggle', 'dropdown');
            jQuery(this).parent().parent().children('a').attr('role', 'button');
            jQuery(this).parent().parent().children('a').attr('aria-haspopup', 'true');
            jQuery(this).parent().parent().children('a').attr('aria-expanded', 'false');
            jQuery(this).parent().addClass('dropdown-menu');

        });

        jQuery(this).children().each(function() {
            jQuery(this).children('span').addClass('hidden-xs');
        });

    });
});