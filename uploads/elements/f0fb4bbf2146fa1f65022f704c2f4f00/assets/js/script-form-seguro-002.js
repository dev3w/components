jQuery(document).ready(function() {
    jQuery("#prev").on("click", function() {
        if (jQuery(".form-seguro-002 .page.active").index() > 0)
            jQuery(".form-seguro-002 .page.active").removeClass("active").prev().addClass("active");
    });
    jQuery("#next").on("click", function() {
        if (jQuery(".form-seguro-002 .page.active").index() < jQuery(".page").length - 1)
            jQuery(".form-seguro-002 .page.active").removeClass("active").next().addClass("active");
    });
    jQuery("#prev-step").on("click", function() {
        if (jQuery(".form-seguro-002 .page.active").index() > 0)
            jQuery(".form-seguro-002 .page.active").removeClass("active").prev().addClass("active");
    });
    jQuery("#next-step").on("click", function() {
        if (jQuery(".form-seguro-002 .page.active").index() < jQuery(".page").length - 1)
            jQuery(".form-seguro-002 .page.active").removeClass("active").next().addClass("active");
    });
});