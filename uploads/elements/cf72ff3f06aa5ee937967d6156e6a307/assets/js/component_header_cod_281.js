jQuery(document).ready(function() {

    $(document).on({
        mouseenter: function() {

            $(this).find('>.dropdown-menu').stop(true, true).delay(200).slideDown(500).fadeIn(500);
            $(this).children('a').addClass('hovered')
        },
        mouseleave: function() {
            $(this).children('a').removeClass('hovered');
            $(this).find('>.dropdown-menu').stop(true, true).delay(200).slideUp(500).fadeOut(500);
        }
    }, ".x_component_header_cod_281 ul.nav li.dropdown");

    $(document).on({
        mouseenter: function() {
            $(this).find('.dropdown-menu').slideDown(500).fadeIn(500);
            $(this).children('a').addClass('hovered')
        },
        mouseleave: function() {
            $(this).find('.dropdown-menu').slideUp(500).fadeOut(500);
            $(this).children('a').removeClass('hovered');

        }
    }, ".x_component_header_cod_281 ul.nav li.dropdown .dropdown-submenu");


    $(document).on('change', '.x_component_header_cod_281 #select-menu', function() {
        var locations = $(this).val();
        if (locations !== '#') {
            window.location.href = $(this).val();
        }

    });

    $(".x_component_header_cod_281 #bs-example-navbar-collapse-1 > ul").each(function() {
        $(this).addClass('nav navbar-nav');
        $(this).find('ul > li').each(function() {
            $(this).parent().parent().addClass("dropdown");
            $(this).parent().parent().children('a').addClass("dropdown-toggle");
            $(this).parent().parent().children('a').attr('data-toggle', 'dropdown');
            $(this).parent().parent().children('a').attr('role', 'button');
            $(this).parent().parent().children('a').attr('aria-haspopup', 'true');
            $(this).parent().parent().children('a').attr('aria-expanded', 'false');
            $(this).parent().addClass('dropdown-menu');
            if ($(this).children().length > 0) {
                $(this).parent().parent().removeClass('dropdown-menu').addClass('dropdown-submenu');
            }
        });
    });

});