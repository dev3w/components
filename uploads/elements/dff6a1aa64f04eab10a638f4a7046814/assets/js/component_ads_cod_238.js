jQuery(document).ready(function(){

	jQuery('.x_component_ads_code_238 .my-carosel').slick({
		slidesToShow: 4,
		slideToMove: 1,
		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 2
			}
		},
		{
			breakpoint: 460,
			settings: {
				slidesToShow: 1
			}
		}]
	});

});