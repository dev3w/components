jQuery(document).ready(function() {
    jQuerywindow = jQuery(window);
    jQuery(function() {
        jQuery(".buscador_013 #slider-range").slider({
            range: true,
            min: 0,
            max: 500,
            values: [10, 300],
            slide: function(event, ui) {
                jQuery("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
        });
        jQuery("#amount").val("$" + jQuery("#slider-range").slider("values", 0) +
            " - $" + $("#slider-range").slider("values", 1));
    });
});