jQuery(document).ready(function() {

    $(document).on({
        mouseenter: function() {

            $(this).find('>.dropdown-menu').stop(true, true).delay(200).slideDown(500).fadeIn(500);
            $(this).children('a').addClass('hovered')
        },
        mouseleave: function() {
            $(this).children('a').removeClass('hovered');
            $(this).find('>.dropdown-menu').stop(true, true).delay(200).slideUp(500).fadeOut(500);
        }
    }, ".menu-m016 ul.nav li.dropdown");

    $(document).on({
            mouseenter: function() {
                $(this).find('.dropdown-menu').slideDown(500).fadeIn(500);
                $(this).children('a').addClass('hovered')
            },
            mouseleave: function() {
                $(this).find('.dropdown-menu').slideUp(500).fadeOut(500);
                $(this).children('a').removeClass('hovered');

            }
        },
        ".menu-m016 ul.nav li.dropdown .dropdown-submenu");


    $(window).resize(function() {
        loadMobileMenu();
    });
    if (jQuery(window).width() < 768) {
        loadMobileMenu();
    }

    function loadMobileMenu() {
        if (jQuery(window).width() < 768) {

            $('.menu-m016 .mobile-menu select').remove();
            var select = '<select class="select-menu form-control visible-xs" id="select-menu"> <option value="">Menu</option>';
            jQuery(".menu-m016 #bs-example-navbar-collapse-1 > ul >li").each(function() {
                var data = $(this).html();
                if ($(data).children().length > 0) {
                    select += '<option value="' + $(data).attr('href') + '">' + $(data).html().trim() + '</option>';
                    $(this).find('ul > li').each(function() {
                        var childrenData = $(this).html();
                        var dashValue = '&ndash;';
                        select += '<option value="' + $(childrenData).attr('href') + '">' + dashValue + $(childrenData).html().trim() + '</option>';
                    });
                } else {
                    select += '<option value="' + $(data).attr('href') + '">' + $(data).html().trim() + '</option>';
                }
            });


            $('.menu-m016 .mobile-menu').append(select);
        }
    }

    $(document).on('change', '.menu-m016 #select-menu', function() {
        var locations = $(this).val();
        if (locations !== '#') {
            window.location.href = $(this).val();
        }

    });

    $(".menu-m016 #bs-example-navbar-collapse-1 > ul").each(function() {
        $(this).addClass('nav navbar-nav menu');
        $(this).find('ul > li').each(function() {
            $(this).parent().parent().addClass("dropdown");
            $(this).parent().parent().children('a').addClass("dropdown-toggle");
            $(this).parent().parent().children('a').attr('data-toggle', 'dropdown');
            $(this).parent().parent().children('a').attr('role', 'button');
            $(this).parent().parent().children('a').attr('aria-haspopup', 'true');
            $(this).parent().parent().children('a').attr('aria-expanded', 'false');
            $(this).parent().addClass('dropdown-menu');
            if ($(this).children().length > 0) {
                $(this).parent().parent().removeClass('dropdown-menu').addClass('dropdown-submenu');
            }
        });
    });

});