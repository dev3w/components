function isInteger(n) {
    return n === +n && n === (n | 0);
}
var addClassMenu = function() {
    jQuery(".menu-m017 .cont-menu > ul").addClass("menu inline-block pull-right");
    jQuery(".menu-m017 .cont-menu-half > ul.menu-half").addClass("menu");
    jQuery(".menu-m017 .cont-menu > ul > li > ul").addClass("mega-menu");
    jQuery(".menu-m017 .cont-menu > ul > li").each(function() {
        jQuery(this).find('ul').each(function() {
            if (jQuery(this).parent('li').length > 0) {
                jQuery(this).parent('li').addClass("has-dropdown");
            }
        });
    });

}
var arrumaMenu = function() {
    var tamanho = jQuery(".menu-m017 .cont-menu > ul > li").length;
    var MetadeTamanho = jQuery(".menu-m017 .cont-menu > ul > li").length / 2;
    jQuery(".menu-m017 .cont-menu > ul > li ").each(function(i) {
        if (!isInteger(MetadeTamanho)) {
            if (i > MetadeTamanho) {
                jQuery(this).each(function(i) {
                    jQuery(this).appendTo('.menu-m017 .menu-half');
                });
            }
        } else {
            if (i > MetadeTamanho - 1) {
                jQuery(this).each(function(i) {
                    jQuery(this).appendTo('.menu-m017 .menu-half');
                });
            }
        }
    });
}
arrumaMenu();
jQuery(document).ready(function() {
    addClassMenu();
    jQuery('.menu-m017 .mobile-toggle').click(function() {
        jQuery('.menu-m017 .nav-bar').toggleClass('nav-open');
        jQuery(this).toggleClass('active');
    });

    jQuery('.menu-m017 .menu li').click(function(e) {
        if (!e) e = window.event;
        e.stopPropagation();
        if (jQuery(this).find('ul').length) {
            jQuery(this).toggleClass('toggle-sub');
        } else {
            jQuery(this).parents('.toggle-sub').removeClass('toggle-sub');
        }
    });

    jQuery('.menu-m017 .menu li a').click(function() {
        if (jQuery(this).hasClass('inner-link')) {
            jQuery(this).closest('.nav-bar').removeClass('nav-open');
        }
    });

    jQuery('.menu-m017 .module.widget-handle').click(function() {
        jQuery(this).toggleClass('toggle-widget-handle');
    });

    jQuery('.menu-m017 .search-widget-handle .search-form input').click(function(e) {
        if (!e) e = window.event;
        e.stopPropagation();
    });
});