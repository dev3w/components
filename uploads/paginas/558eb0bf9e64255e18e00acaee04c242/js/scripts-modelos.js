/* * noConflict */
var $modelos = jQuery.noConflict();


$modelos(window).load(function() {

    if ($modelos().waypoint) {
        // animation effect
        $modelos('.zkmodelos-zk001 .animated').waypoint(function() {
            var type = $modelos(this).data("animation-type");
            if (typeof type == "undefined" || type == false) {
                type = "fadeIn";
            }
            $modelos(this).addClass(type);

            var duration = $modelos(this).data("animation-duration");
            if (typeof duration == "undefined" || duration == false) {
                duration = "1";
            }
            $modelos(this).css("animation-duration", duration + "s");

            var delay = $modelos(this).data("animation-delay");
            if (typeof delay != "undefined" && delay != false) {
                $modelos(this).css("animation-delay", delay + "s");
            }

            $modelos(this).css("visibility", "visible");

            setTimeout(function() { $modelos.waypoints('refresh'); }, 1000);
        }, {
            triggerOnce: true,
            offset: 'bottom-in-view'
        });
    }



});