/* * noConflict */

var $marcas = jQuery.noConflict();


$marcas(window).load(function() {

    if ($marcas().waypoint) {
        // animation effect
        $marcas('.zkmarcas-zk001 .animated').waypoint(function() {
            var type = $marcas(this).data("animation-type");
            if (typeof type == "undefined" || type == false) {
                type = "fadeIn";
            }
            $marcas(this).addClass(type);

            var duration = $marcas(this).data("animation-duration");
            if (typeof duration == "undefined" || duration == false) {
                duration = "1";
            }
            $marcas(this).css("animation-duration", duration + "s");

            var delay = $marcas(this).data("animation-delay");
            if (typeof delay != "undefined" && delay != false) {
                $marcas(this).css("animation-delay", delay + "s");
            }

            $marcas(this).css("visibility", "visible");

            setTimeout(function() { $marcas.waypoints('refresh'); }, 1000);
        }, {
            triggerOnce: true,
            offset: 'bottom-in-view'
        });
    }
});