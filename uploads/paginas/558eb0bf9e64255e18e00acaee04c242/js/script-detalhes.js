var $zkdetalhes = jQuery.noConflict();

/* display photo gallery */
function displayPhotoGallery($item) {
    if (!$zkdetalhes.fn.flexslider || $item.length < 1 || $item.is(":hidden")) {
        return;
    }
    var dataAnimation = $item.data("animation");
    var dataSync = $item.data("sync");
    if (typeof dataAnimation == "undefined") {
        dataAnimation = "slide";
    }
    var dataFixPos = $item.data("fix-control-nav-pos");
    var callFunc = $item.data("func-on-start");

    $item.flexslider({
        animation: dataAnimation,
        controlNav: true,
        animationLoop: true,
        slideshow: true,
        pauseOnHover: true,
        sync: dataSync,
        start: function(slider) {
            if (typeof dataFixPos != "undefined" && dataFixPos == "1") {
                var height = $zkdetalhes(slider).find(".zkdetalhes-zk001 .slides img").height();
                $zkdetalhes(slider).find(".flex-control-nav .color-default").css("top", (height - 44) + "px");
            }
            if (typeof callFunc != "undefined") {
                try {
                    eval(callFunc + "();");
                } catch (e) {}
            }
        },
    });
}

/* display image carousel */
function displayImageCarousel($item) {
    if (!$zkdetalhes.fn.flexslider || $item.length < 1 || $item.is(":hidden")) {
        return;
    }
    var dataItemWidth = $item.data("item-width");
    var dataItemMargin = $item.data("item-margin");
    var dataSync = $item.data("sync");
    if (typeof dataAnimation == "undefined") {
        dataAnimation = "slide";
    }
    if (typeof dataItemWidth == "undefined") {
        dataItemWidth = 70;
    }
    if (typeof dataItemMargin == "undefined") {
        dataItemMargin = 10;
    }
    dataItemWidth = parseInt(dataItemWidth, 10);
    dataItemMargin = parseInt(dataItemMargin, 10);

    var dataAnimationLoop = true;
    var dataSlideshow = false;
    if (typeof dataSync == "undefined") {
        dataSync = "";
        //dataAnimationLoop = true;
        dataSlideshow = true;
    }
    $item.flexslider({
        animation: dataAnimation,
        controlNav: true,
        animationLoop: dataAnimationLoop,
        slideshow: dataSlideshow,
        itemWidth: dataItemWidth,
        itemMargin: dataItemMargin,
        minItems: 2,
        pauseOnHover: true,
        asNavFor: dataSync,
        start: function(slider) {
            if (dataSync != "") {
                $zkdetalhes(slider).find(".zkdetalhes-zk001 .slides > li").height(dataItemWidth);
                $zkdetalhes(slider).find(".zkdetalhes-zk001 .slides > li > img").each(function() {
                    if ($zkdetalhes(this).width() < 1) {
                        $zkdetalhes(this).load(function() {
                            $zkdetalhes(this).parent().middleblock();
                        });
                    } else {
                        $zkdetalhes(this).parent().middleblock();
                    }
                });
            } else {
                $zkdetalhes(slider).find(".zkdetalhes-zk001 .middle-block img, .middle-block .middle-item").each(function() {
                    if ($zkdetalhes(this).width() < 1) {
                        $zkdetalhes(this).load(function() {
                            $zkdetalhes(this).closest(".zkdetalhes-zk001 .middle-block").middleblock();
                        });
                    } else {
                        $zkdetalhes(this).closest(".zkdetalhes-zk001 .middle-block").middleblock();
                    }
                });
            }
        },
        after: function(slider) {
            if (slider.currentItem == 0) {
                target = 0;
                if (slider.transitions) {
                    target = (slider.vars.direction === "vertical") ? "translate3d(0," + target + ",0)" : "translate3d(" + target + ",0,0)";
                    slider.container.css("-" + slider.pfx + "-transition-duration", "0s");
                    slider.container.css("transition-duration", "0s");
                }
                slider.args[slider.prop] = target;
                slider.container.css(slider.args);
                slider.container.css('transform', target);
            }
        }

    });
}
// Mobile search
if ($zkdetalhes('.zkdetalhes-zk001 #mobile-search-tabs').length > 0) {
    var mobile_search_tabs_slider = $zkdetalhes('.zkdetalhes-zk001 #mobile-search-tabs').bxSlider({
        mode: 'fade',
        infiniteLoop: false,
        hideControlOnEnd: true,
        touchEnabled: true,
        pager: false,
        onSlideAfter: function($slideElement, oldIndex, newIndex) {
            $zkdetalhes('a[href="' + $zkdetalhes($slideElement).children("a").attr("href") + '"]').tab('show');
        }
    });
}
$zkdetalhes(window).load(function() {
    /* photo gallery and slideshow */
    // photo gallery with thumbnail
    $zkdetalhes('.zkdetalhes-zk001 .image-carousel').each(function() {
        displayImageCarousel($zkdetalhes(this));
    });
    $zkdetalhes('.zkdetalhes-zk001 .photo-gallery').each(function() {
        displayPhotoGallery($zkdetalhes(this));
    });
});