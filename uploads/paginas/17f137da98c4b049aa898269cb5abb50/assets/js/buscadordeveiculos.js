jQuery(document).ready(function() {
	
    jQuery('#sel_mostrar_pp').change(function() {
        window.location = jQuery("#sel_mostrar_pp").val();
        return true;
    });
    
    jQuery("input[name='compare']").click(function() {
        var allVals = [];
        jQuery("input[name='compare']:checked").each(function() {
            allVals.push(jQuery(this).val());
        });
        if (allVals.length >= 3)
        {
            jQuery("input[name='compare']").each(function() {
                if (!jQuery(this).is(':checked'))
                {
                    jQuery(this).attr('disabled', true);
                }
            });
        }
        else
        {
            jQuery("input[name='compare']").each(function() {
                jQuery(this).attr('disabled', false);
            });
        }
    });
    
    jQuery("#btnCompare1").click(function() {
    	
        var allVals = [];
        jQuery("input[name='compare']:checked").each(function() {
            allVals.push(jQuery(this).val());
        });

        var valores = '';
        var get = '';
        
        for (i=0;i<allVals.length;i++)
        {
            valores += allVals[i] + ',';
            
            if (get != '')
                get += '_';
            
            get += allVals[i];
        }
        valores = valores.substring(0,(valores.length - 1));
        valores_arr = valores.split(",")
        if(valores_arr.length < 2) 
        {
            alert('Por favor, selecione mais de um veículo para comparação.');
            return false;
        }

        if(valores_arr.length > 3) 
        {
            alert('Desculpe! Não é possível comparar mais de 3 veículos.');
            return false;
        }

        //alert('compare?veiculos='+get);

        location.href = 'compare?veiculos='+get;
	});
    
	jQuery("#btnCompare2").click(function() {
        var allVals = [];
        jQuery("input[name='compare']:checked").each(function() {
            allVals.push(jQuery(this).val());
        });

        var valores = '';
        var get = '';
        
        for (i=0;i<allVals.length;i++)
        {
            valores += allVals[i] + ',';
            
            if (get != '')
                get += '_';
            
            get += allVals[i];
        }
        valores = valores.substring(0,(valores.length - 1));
        valores_arr = valores.split(",")
        if(valores_arr.length < 2) 
        {
            alert('Por favor, selecione mais de um veículo para comparação.');
            return false;
        }

        if(valores_arr.length > 3) 
        {
            alert('Desculpe! Não é possível comparar mais de 3 veículos.');
            return false;
        }

        //alert('compare?veiculos='+get);

        location.href = 'compare?veiculos='+get;
	});
});