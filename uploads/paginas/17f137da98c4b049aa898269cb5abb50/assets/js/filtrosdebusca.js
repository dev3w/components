jQuery(function() {
	var menu_ul 	= jQuery('.filtrosdebusca > .refine > #wrapper > ul.menu_filtro > li > ul'), 
		menu_a 		= jQuery('.filtrosdebusca > .refine > #wrapper > ul.menu_filtro > li > a'),
		menu_a_span = jQuery('.filtrosdebusca > .refine > #wrapper > ul.menu_filtro > li > a > span');

	menu_ul.hide();

	menu_a.click(function(e) {
		e.preventDefault();

		if(!jQuery(this).hasClass('active')) 
		{
			//menu_a.removeClass('active');
			//menu_ul.filter(':visible').slideUp('normal');
			jQuery(this).addClass('active').next().stop(true, true).slideDown('normal');
		}
		else 
		{
			jQuery(this).removeClass('active');
			jQuery(this).next().stop(true, true).slideUp('normal');
		}
	});

	jQuery(".filtrosdebusca > .refine > .btn-filtro").click(function() {
        if(jQuery(".filtrosdebusca > .refine").hasClass("side-fechado")) {
            jQuery(".filtrosdebusca > .refine").animate( {
                left: "0px",
            }, 100, function() {
                jQuery(".filtrosdebusca > .refine").removeClass("side-fechado");
            });
        }
        else 
        {
            jQuery(".filtrosdebusca > .refine").animate( {
                left: "-200px",
            }, 100, function() {
                jQuery(".filtrosdebusca > .refine").addClass("side-fechado");
            });
        }
    });
});