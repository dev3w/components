jQuery(window).resize(function() {
    // linkMapa();
});

jQuery(document).ready(function() {
    // linkMapa();

    jQuery("#dadosLoja .proposta > h5").click(function(){
        var propostaTitulo  = jQuery("#dadosLoja .proposta > h5");
        var finanTitulo     = jQuery("#dadosLoja .financiamento > h5");

        var proposta        = jQuery("#dadosLoja .proposta > .formProp");
        var financiamento   = jQuery("#dadosLoja .financiamento > .formFin");

        proposta.slideDown('fast');
        financiamento.slideUp('fast');

        propostaTitulo.parent().addClass('active');
        finanTitulo.parent().removeClass('active');
    });

    jQuery("#dadosLoja .financiamento > h5").click(function(){
        var propostaTitulo  = jQuery("#dadosLoja .proposta > h5");
        var finanTitulo     = jQuery("#dadosLoja .financiamento > h5");

        var proposta        = jQuery("#dadosLoja .proposta > .formProp");
        var financiamento   = jQuery("#dadosLoja .financiamento > .formFin");

        financiamento.slideDown('fast');
        proposta.slideUp('fast');

        propostaTitulo.parent().removeClass('active');
        finanTitulo.parent().addClass('active');
    });

    jQuery('input#entradaFin').on("blur", function()
    {
        var preco   = jQuery('#dadosVeiculo > h4').html()+' ';
        preco       = getMoney(preco);

        var entrada = jQuery(this).val();
        entrada     = getMoney(entrada);

        var perc    = (preco*10)/100;

        if(entrada<perc)
        {
            alert("A entrada não pode ser menor que 10% do valor do veículo");
            jQuery(this).val(formatReal(perc));
        }
    });

    jQuery('#imageBig').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#imageNav'
    });

    jQuery('#imageNav').slick({
        slidesToShow: 30,
        slidesToScroll: 0,
        asNavFor: '#imageBig',
        centerMode: false,
        focusOnSelect: false
    });

    jQuery('#imageNav > .slick-list > .slick-track > .slick-slide').on('click', function (event) {
        if(!jQuery(this).attr('disabled'))
        {
            jQuery('#imageBig').slick('slickGoTo', jQuery(this).data('slickIndex'));
        }
    });
});

/*function linkMapa()
{
    var wLargura = jQuery(window).width();
    if(wLargura<=768)
    {
        try {
            jQuery("li.estoqueMapa > a.mapa").cymon_box.destroy();
        } catch (exception) {}
    }
    else
    {
        jQuery("li.estoqueMapa > a.mapa").cymon_box({
            'largura'   : 80,
            'altura'    : 80,
            'tipo'      : '%'
        });
    }
}*/

function getMoney(str)
{
    return parseInt( str.replace(/[\D]+/g,'') );
}
function formatReal(int)
{
    var tmp = int+'';
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");

    if( tmp.length > 6 )
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
     
    return tmp;
}

function debug(str) {
    if(window.console && window.console.log && jQuery.browser.mozilla) {
        console.log(str);
    } else {
        jQuery('#debug').show().val(jQuery('#debug').val() + str +'\n');
    }
}
